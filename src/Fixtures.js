import React from "react";
import { getDateFromWeekId } from "./Mrha.Service/getFixtures";

export const Fixtures = props => {
  var fixtures = props.fixtures.map(fixtureData => (
    <Fixture key={fixtureData.Home} data={fixtureData} />
  ));

  var fixturesNavigation = props.context ? (
    <FixtureNavigation
      gameWeek={props.context.gameWeek}
      updateGameWeek={props.context.updateGameWeek}
    />
  ) : null;

  return props.fixtures ? (
    <div>
      <table width="100%">
        <tbody>
          {fixturesNavigation}
          {fixtures}
        </tbody>
      </table>
    </div>
  ) : null;
};

const FixtureNavigation = props => {
  return (
    <tr>
      <th onClick={() => props.updateGameWeek(-1)}>
        <img
          src="/left_arrow.png"
          alt="previous week's fixtures"
          style={{ marginRight: "10px" }}
          className="icon"
        />
        Prev
      </th>
      <th>{getDateFromWeekId(props.gameWeek)}</th>
      <th onClick={() => props.updateGameWeek(1)}>
        Next
        <img
          src="/left_arrow.png"
          alt="next week's fixtures"
          style={{ marginLeft: "10px" }}
          className="icon rotate180"
        />
      </th>
    </tr>
  );
};

const Fixture = props => {
  return (
    <tr>
      <th width="40%">
        <a href={props.data.HomeUrl}>{props.data.Home}</a>
      </th>
      {props.data.Score ? (
        <th style={{ fontWeight: "700" }}>{props.data.Score}</th>
      ) : (
        <th>
          <a href={props.data.VenueUrl}>{props.data.Time}</a>
        </th>
      )}
      <th width="40%">
        <a href={props.data.AwayUrl}>{props.data.Away}</a>
      </th>
    </tr>
  );
};
