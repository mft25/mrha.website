import axios from "axios";
import cheerio from "cheerio";

export async function getTable(league, callback) {
  return axios
    .get("http://mrha.co.uk/leaguetables.asp", {
      params: {
        LeagueId: league.LeagueId,
        DivisionId: league.DivisionId
      }
    })
    .then(res => getTableFromHtml(res.data))
    .then(callback);
}

function getTableFromHtml(tableHtml) {
  let $ = cheerio.load(tableHtml);

  var teamRows = $("center table").find(".PlainRow");

  var tableRows = [];
  teamRows.each((i, row) => {
    var teamRow = $(row).children("td");

    tableRows.push(getTeamDataFromTeamRow($, teamRow));
  });

  return tableRows;
}

function getTeamDataFromTeamRow($, teamRow) {
  var name = getCellValue($, teamRow, 1);
  var p = getCellValue($, teamRow, 2);
  var w = getCellValue($, teamRow, 3);
  var d = getCellValue($, teamRow, 4);
  var l = getCellValue($, teamRow, 5);
  var gf = getCellValue($, teamRow, 6);
  var ga = getCellValue($, teamRow, 7);
  var gd = getCellValue($, teamRow, 8);
  var pts = getCellValue($, teamRow, 9);

  return {
    Name: name,
    Played: p,
    Won: w,
    Drawn: d,
    Lost: l,
    GoalsFor: gf,
    GoalsAgainst: ga,
    GoalDifference: gd,
    Points: pts
  };
}

function getCellValue($, row, columnIndex) {
  return $(row[columnIndex])
    .first()
    .text()
    .trim();
}
