import axios from "axios";
import cheerio from "cheerio";
import weeks from "./weekIds.json";

export async function getFixtures(league, weekId, callback) {
  return axios
    .get("http://mrha.co.uk/mwDivisionFixtures.asp", {
      params: {
        LeagueId: league.LeagueId,
        DivisionId: league.DivisionId,
        WeekId: weekId
      }
    })
    .then(res => getFixturesFromHtml(res.data))
    .then(callback);
}

export function getCurrentWeekId() {
  var weekId = 0;

  weeks.forEach(week => {
    if (weekId === 0 && isDateTodayOrInFuture(week.Date)) {
      weekId = week.WeekId;
    }
  });

  return weekId;
}

export function getDateFromWeekId(weekId) {
  var date = weeks.find(week => week.WeekId === weekId).Date;

  return new Date(date).toLocaleDateString("en-GB", {
    day: "numeric",
    month: "short",
    year: "numeric"
  });
}

function isDateTodayOrInFuture(dateString) {
  var todaysDateString = new Date().toJSON().slice(0, 10);
  var todaysDate = new Date(todaysDateString);
  var date = new Date(dateString);

  return date.getTime() >= todaysDate.getTime();
}

function getFixturesFromHtml(fixturesHtml) {
  let $ = cheerio.load(fixturesHtml);

  var rows = $("table")
    .first() // only the first table has fixtures
    .find("tr")
    .slice(1); // the first row is the header - https://stackoverflow.com/questions/38096687/remove-first-element-from-array-and-return-the-array-minus-the-first-element

  var fixtures = [];
  rows.each((i, row) => {
    var fixtureHtml = $(row).children("td");

    fixtures.push(getFixtureDataFromHtml($, fixtureHtml));
  });

  return fixtures;
}

function getFixtureDataFromHtml($, fixtureHtml) {
  var home = getCellValue($, fixtureHtml, 0);
  var homeUrl = getCellHref($, fixtureHtml, 0);
  var score = getCellValue($, fixtureHtml, 1);
  var away = getCellValue($, fixtureHtml, 2);
  var awayUrl = getCellHref($, fixtureHtml, 2);
  var time = getCellValue($, fixtureHtml, 3);
  var venue = getCellValue($, fixtureHtml, 4);
  var venueUrl = getCellHref($, fixtureHtml, 4);

  if (score.indexOf("To be played") >= 0) {
    score = null;
  }

  return {
    Home: home,
    HomeUrl: homeUrl,
    Away: away,
    AwayUrl: awayUrl,
    Score: score,
    Time: time,
    Venue: venue,
    VenueUrl: venueUrl
  };
}

function getCellValue($, row, columnIndex) {
  return $(row[columnIndex])
    .first()
    .text()
    .trim();
}

function getCellHref($, row, columnIndex) {
  return (
    "http://mrha.co.uk/" +
    $(row[columnIndex])
      .first()
      .children()
      .attr("href")
  );
}
