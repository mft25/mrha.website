import { getFixtures } from "./getFixtures";
import { getTable } from "./getTable";

export async function getLeague(leagueKey, weekId) {
  var league = {
    Table: null,
    Fixtures: null
  };

  await getTable(leagueKey).then(res => {
    league.Table = res;
  });

  await getFixtures(leagueKey, weekId).then(res => {
    league.Fixtures = res;
  });

  return league;
}
