import React from "react";
import "./App.css";
import { League } from "./League";
import leaguesJson from "./leagues.json";

function App() {
  var leagues = leaguesJson.map(league => (
    <League league={league} key={league.TeamId} />
  ));

  return (
    <div className="App">
      <div className="App-header">{leagues}</div>
    </div>
  );
}

export default App;
