import React from "react";
import { getLeague as getMrhaLeague } from "./Mrha.Service/mrha.Service";
import { getCurrentWeekId } from "./Mrha.Service/getFixtures";
import { getLeague as getFixturesLiveLeague } from "./FixturesLive.Service/fixturesLive.Service";
import { LeagueHeader } from "./LeagueHeader";
import { Fixtures } from "./Fixtures";
import { Table } from "./Table";

export class League extends React.Component {
  MRHA_LEAGUE = 1;
  FIXTURES_LIVE_LEAGUE = 2;

  state = {
    league: null,
    collapsed: true,
    context: {
      mrha: {
        weekId: getCurrentWeekId()
      }
    }
  };

  componentDidMount() {
    var shouldGetLeague = false;

    // If this league is for the default team (ie. the team in the URL) then load the league and open it.
    if (window.location.hash.indexOf(this.props.league.TeamId) > 0) {
      shouldGetLeague = true;
      this.toggleCollapsedState();
    }

    // Fixtures Live leagues must be loaded immediately.
    if (this.props.league.LeagueType === this.FIXTURES_LIVE_LEAGUE) {
      shouldGetLeague = true;
    }

    if (shouldGetLeague) {
      this.getLeague();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      !this.state.league ||
      this.state.context.mrha.weekId !== prevState.context.mrha.weekId
    ) {
      this.getLeague();
    }
  }

  getLeague = () => {
    if (this.props.league.LeagueType === this.MRHA_LEAGUE) {
      getMrhaLeague(this.props.league, this.state.context.mrha.weekId).then(
        res => {
          this.setLeagueState(res);
        }
      );
    }

    if (this.props.league.LeagueType === this.FIXTURES_LIVE_LEAGUE) {
      getFixturesLiveLeague(
        this.props.league,
        this.setLeagueState,
        () => !this.state.league
      );
    }
  };

  setLeagueState = league => {
    this.setState({
      league: league
    });
  };

  toggleCollapsedState = () => {
    if (this.state.collapsed && !window.location.hash) {
      this.setDefaultTeam();
    }

    this.setState({
      collapsed: !this.state.collapsed
    });
  };

  setDefaultTeam() {
    if (window.history.pushState) {
      var newurl = `${window.location.origin}${window.location.pathname}#${this.props.league.TeamId}`;
      window.history.pushState({ path: newurl }, "", newurl);
    }
  }

  updateGameWeek = diff => {
    this.setState({
      context: {
        mrha: {
          weekId: this.state.context.mrha.weekId + diff
        }
      }
    });
  };

  render() {
    // Fixtures Live denies cross-origin requests, so instead use their embed functionality to load their data into a
    // div with id "fl" and then use that to extract the league data required.
    if (
      this.props.league.LeagueType === this.FIXTURES_LIVE_LEAGUE &&
      !this.state.league
    ) {
      return (
        <div>
          <LeagueHeader
            leagueName={this.props.league.LeagueName}
            onToggle={this.toggleCollapsedState}
          />
          <div id="fl" className="hide" />
        </div>
      );
    }

    var context = null;
    if (this.props.league.LeagueType === this.MRHA_LEAGUE) {
      context = {
        gameWeek: this.state.context.mrha.weekId,
        updateGameWeek: this.updateGameWeek
      };
    }

    return (
      <div>
        <LeagueHeader
          leagueName={this.props.league.LeagueName}
          onToggle={this.toggleCollapsedState}
        />
        {this.state.collapsed || !this.state.league ? null : (
          <div>
            <Fixtures fixtures={this.state.league.Fixtures} context={context} />
            <div style={{ margin: "20px 0px" }} />
            <Table table={this.state.league.Table} />
          </div>
        )}
        <div style={{ margin: "20px 0px" }} />
      </div>
    );
  }
}
