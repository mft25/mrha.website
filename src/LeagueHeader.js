import React from "react";

export const LeagueHeader = props => {
  return (
    <p onClick={props.onToggle}>
      <img
        src="/double_arrow.png"
        alt="toggle league presence"
        className="icon rotate90"
        style={{ marginRight: "10px" }}
      />
      {props.leagueName}
    </p>
  );
};
