import React from "react";

export class Table extends React.Component {
  state = {
    wide: false,
    setExplicitly: false
  };

  SHOW_FULL_TABLE_AT_WIDTH = 600;

  explicitlySetTableWidth = () => {
    this.setState({
      wide: !this.state.wide,
      setExplicitly: true
    });
  };

  updateTableWidth = () => {
    if (this.state.setExplicitly) {
      return;
    }

    var width =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.getElementsByTagName("body")[0].clientWidth;

    var wide = width >= this.SHOW_FULL_TABLE_AT_WIDTH;
    if (this.state.wide !== wide) {
      this.setState({ wide: wide });
    }
  };

  componentDidMount() {
    this.updateTableWidth();
    window.addEventListener("resize", this.updateTableWidth);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this.updateTableWidth);
  }

  render() {
    var teams = this.props.table.map(teamData => (
      <TableRow key={teamData.Name} data={teamData} wide={this.state.wide} />
    ));

    return this.props.table ? (
      <div>
        <table width="100%">
          <tbody>
            <TableHeader
              wide={this.state.wide}
              changeState={this.explicitlySetTableWidth}
            />
            {teams}
          </tbody>
        </table>
      </div>
    ) : null;
  }
}

function TableHeader(props) {
  var className = props.wide ? "" : "hide";

  return (
    <tr>
      <th width="50%">
        <img
          src="/double_arrow.png"
          alt="toggle table size"
          className="icon"
          onClick={props.changeState}
        />
      </th>
      <th>P</th>
      <th className={className}>W</th>
      <th className={className}>D</th>
      <th className={className}>L</th>
      <th className={className}>GF</th>
      <th className={className}>GA</th>
      <th>GD</th>
      <th>Pts</th>
    </tr>
  );
}

function TableRow(props) {
  var className = props.wide ? "" : "hide";

  return (
    <tr>
      <th width="50%">{props.data.Name}</th>
      <th>{props.data.Played}</th>
      <th className={className}>{props.data.Won}</th>
      <th className={className}>{props.data.Drawn}</th>
      <th className={className}>{props.data.Lost}</th>
      <th className={className}>{props.data.GoalsFor}</th>
      <th className={className}>{props.data.GoalsAgainst}</th>
      <th>{props.data.GoalDifference}</th>
      <th>{props.data.Points}</th>
    </tr>
  );
}
