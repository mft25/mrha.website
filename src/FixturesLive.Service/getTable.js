import cheerio from "cheerio";

export function getTableFromHtml(tableHtml, setLeagueCallback) {
  let $ = cheerio.load(tableHtml);

  var flTableRows = $("table.leagueTable tbody").find("tr");

  var tableRows = [];
  flTableRows.each((i, row) => {
    // Only rows with 21 columns are actual teams in the table.
    if (row.children.length === 21) {
      tableRows.push(getTeamDataFromTeamRow(row));
    }
  });

  if (tableRows.length === 0) {
    return;
  }

  setLeagueCallback({
    Fixtures: [],
    Table: tableRows
  });
}

function getTeamDataFromTeamRow(row) {
  var name = row.children[2].children[0].children["0"].data;
  var p = getCellValue(row, 3);
  var w = getCellValue(row, 14);
  var d = getCellValue(row, 15);
  var l = getCellValue(row, 16);
  var gf = getCellValue(row, 17);
  var ga = getCellValue(row, 18);
  var gd = getCellValue(row, 19);
  var pts = getCellValue(row, 20);

  return {
    Name: name,
    Played: p,
    Won: w,
    Drawn: d,
    Lost: l,
    GoalsFor: gf,
    GoalsAgainst: ga,
    GoalDifference: gd,
    Points: pts
  };
}

function getCellValue(row, columnIndex) {
  return row.children[columnIndex].children[0].data;
}
