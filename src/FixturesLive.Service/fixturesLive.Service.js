import { getTableFromHtml } from "./getTable";

export async function getLeague(leagueKey, setLeagueCallback, updateRequired) {
  appendCompetitionScript(leagueKey.CompetitionId);

  subscribeToDataChanges(setLeagueCallback, updateRequired);
}

function appendCompetitionScript(competitionId) {
  const script = document.createElement("script");

  script.innerHTML = `var fPassKey = '', fStartPage = 'STATZONE_COMP:${competitionId}:1';`;
  script.async = true;

  document.body.appendChild(script);
}

function subscribeToDataChanges(setLeagueCallback, updateRequired) {
  var flData = document.getElementById("fl");

  var observer = new MutationObserver(function(mutations) {
    if (updateRequired()) {
      extractLeagueData(setLeagueCallback);
    }
  });

  var config = { attributes: true, childList: true, characterData: true };

  observer.observe(flData, config);
}

function extractLeagueData(setLeagueCallback) {
  const flData = document.getElementById("fl");

  getTableFromHtml(flData.innerHTML, setLeagueCallback);
}
